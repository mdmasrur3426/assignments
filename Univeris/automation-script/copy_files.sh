#!/bin/bash

sourceDirectory=/home/masrur/git/git/bashscripts/test_dir
c=0;
for file in $sourceDirectory/*; do
    dir=${file%FSRV.Z01}
    dir=${dir##*.}
    year=${dir:4:2}
    month=${dir:2:2}
    day=${dir:0:2}
    mkdir -p "./$year/$month/$day"
    mv -iv "$file" "./$year/$month/$day" && ((c++))
done
echo $c file were moved
