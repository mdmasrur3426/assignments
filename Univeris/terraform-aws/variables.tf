variable "key_name" {
  default = "ec2Key"      
}

# Change the path to terraform root
variable "base_path" {
  default = "/change/the/path/to/terraform/root"
}