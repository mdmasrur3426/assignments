output "wordpress_ip" {
  value = "${aws_instance.wordpress.*.public_ip}"
}

output "bastion_ip" {
  value = "${aws_instance.bastion_host.*.public_ip}"
}